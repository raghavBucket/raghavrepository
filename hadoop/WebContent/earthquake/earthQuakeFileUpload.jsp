<html>
<head>
<title>Earth Quake</title>
<link rel="stylesheet" type="text/css" href="/hadoop/css/style.css">
</head>
<body style="background-color: #faf2e4;">
	<div style="background-color: #faf2e4;">
		<h1>
			<font color="red">
				<p style="border-style: double; text-align: center;">
					<strong>Welcome to Hadoop's Earth Quake Analysis</strong>
				</p>
			</font>
		</h1>
	</div>
	<br />
	<div
		style="margin-right: 960px; border-style: double; text-align: left; background-color: #FFC0CB;">
		<div style="margin-left: 30px;">
			<br />
			<p style="text-align: left;">
				<strong>Please Upload the file</strong>
			</p>

			<form method="POST" action="saveFile.jsp"
				enctype="multipart/form-data">
				File upload: <input type="file" name="file"><br /> <br />
				<input type="submit" value="Upload"> Press here to upload
				the file!
			</form>
			<br />
		</div>
	</div>
	<p style="text-align: left;">
	<h3>Querying the highest earth quake Magnitude for each region</h3>
	The System has the storage of huge historical data consists of several
	attributes in files which were collected from various measuring
	calculator devices during earth quake . The input is raw data file
	listing earthquakes by region, magnitude and other information.
	<br />
	<br />
	<strong>Requirement</strong> The user has to process input file to
	find the maximum magnitude quake reading for every region
	</p>
	<div class="article-content">
		<a href="/hadoop"> Go to Home Back </a>
	</div>
</body>
</html>