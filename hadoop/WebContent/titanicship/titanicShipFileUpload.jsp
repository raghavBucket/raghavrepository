<html>
<head>
<title>Word Count Example</title>
<link rel="stylesheet" type="text/css" href="/hadoop/css/style.css">
</head>
<body style="background-color: #faf2e4;">
	<div style="background-color: #faf2e4;">
		<h1>
			<font color="red">
				<p style="border-style: double; text-align: center;">
					<strong>Welcome to Hadoop's Titanic Ship Disaster Analysis</strong>
				</p>
			</font>
		</h1>
	</div>
	<br />
	<div
		style="margin-right: 960px; border-style: double; text-align: left; background-color: #FFC0CB;">
		<div style="margin-left: 30px;">
			<br />
			<p style="text-align: left;">
				<strong>Please Upload the file</strong>
			</p>

			<form method="POST" action="saveFile.jsp"
				enctype="multipart/form-data">
				File upload: <input type="file" name="file"><br /> <br />
				<input type="submit" value="Upload"> Press here to upload
				the file!
			</form>
			<br />
		</div>
	</div>
	<p style="text-align: left;">
	<h3>Querying the Average Age of Affected People</h3>
	Titanic, largest ship which has 46,000 ton weight, sank On April 14,
	1912, into the depth of 13,000 feet in a matter of 3 hours during its
	maiden voyage, route to New York City from Southampton, England, and
	about 1,500 passengers were died.
	<br />
	<br />
	<strong>Requirement</strong> The user has to find the average age of
	the people who died with respect to their genders (both male and
	female).
	</p>
	<br />

	<div class="article-content">
		<a href="/hadoop"> Go to Home Back </a>
	</div>
</body>
</html>