<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*, javax.servlet.*"%>
<%@ page import="javax.servlet.http.*"%>
<%@page import="com.mapreduce.titanic.ship.analysis.HadoopConnector"%>
<%@ page import="org.apache.commons.fileupload.*"%>
<%@ page import="org.apache.commons.fileupload.disk.*"%>
<%@ page import="org.apache.commons.fileupload.servlet.*"%>
<%@ page import="org.apache.commons.io.output.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Word Count File Upload</title>
</head>
<body>
	<%
		final String UPLOAD_DIR = "uploadedFiles";
		String hadoopPath = "";
		String applicationPath = getServletContext().getRealPath("");
		String uploadPath = applicationPath + File.separator + UPLOAD_DIR;
		File file = null;
		int maxFileSize = 5000 * 1024;
		int maxMemSize = 5000 * 1024;
		ServletContext context = pageContext.getServletContext();
		String filePath = context.getInitParameter("file-upload");
		String contentType = request.getContentType();

		if ((contentType.indexOf("multipart/form-data") >= 0)) {
			DiskFileItemFactory factory = new DiskFileItemFactory();
			factory.setSizeThreshold(maxMemSize);
			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setSizeMax(maxFileSize);

			try {
				List<FileItem> fileItems = upload.parseRequest(request);
				Iterator<FileItem> i = fileItems.iterator();
				while (i.hasNext()) {
					FileItem fi = (FileItem) i.next();
					if (!fi.isFormField()) {
						String fieldName = fi.getFieldName();
						String fileName = fi.getName();
						boolean isInMemory = fi.isInMemory();
						long sizeInBytes = fi.getSize();
						System.out.println(fi);
						fileName = fi.getName();
						System.out.println(fileName);
						if (fileName != null) {
							int index = fileName.lastIndexOf("/");
							if (index > 0) {
								fileName = fileName.substring(index + 1);
								file = new File(uploadPath + "/" + fileName);
							} else {
								index = fileName.lastIndexOf("\\");
								System.out.println(index);
								if (index > 0) {
									fileName = fileName.substring(index + 1);
									file = new File(uploadPath + "\\" + fileName);
								} else {
									file = new File(uploadPath + "\\" + fileName);
								}
							}
						}
						String filPath = uploadPath + "\\" + fileName;
						System.out.println(filPath);
						if (file != null) {
							File fileUploadDirectory = new File(uploadPath);
							if (!fileUploadDirectory.exists()) {
								fileUploadDirectory.mkdirs();
							}

							fi.write(file);
							HadoopConnector hs = new HadoopConnector();
							hadoopPath = hs.savetoHdfs(filPath,fileName);
							out.println("Uploaded Filename: " + fileName + "<br>");
						}
					}
				}
			} catch (Exception ex) {
				System.out.println(ex);
			}
		} 
	%>
	<jsp:forward page="titanicShipResults.jsp">
		<jsp:param name="hadoopPath" value='<%=hadoopPath%>' />
	</jsp:forward>
</body>
</html>