<html>
<head>
<title>Word Count Example</title>
<link rel="stylesheet" type="text/css" href="/hadoop/css/style.css">
</head>
<body style="background-color: #faf2e4;">
	<div style="background-color: #faf2e4;">
		<h1>
			<font color="red">
				<p style="border-style: double; text-align: center;">
					<strong>Welcome to Hadoop's Word Count Analysis</strong>
				</p>
			</font>
		</h1>
	</div>
	<br />
	<div
		style="margin-right: 960px; border-style: double; text-align: left; background-color: #FFC0CB;">
		<div style="margin-left: 30px;">
			<br />
			<p style="text-align: left;">
				<strong>Please Upload the file</strong>
			</p>

			<form method="POST" action="saveFile.jsp"
				enctype="multipart/form-data">
				File upload: <input type="file" name="file"><br /> <br />
				<input type="submit" value="Upload"> Press here to upload
				the file!
			</form>
			<br />
		</div>
	</div>
	<p style="text-align: left;">
	<h3>Querying the occurrences of each word</h3>
	The System has the storage of huge collection of words in a file which
	were collected from various sources.
	<br />
	<br />
	<strong>Requirement</strong>
	<p>The user has to find the total count for each word in a file.</p>
	<br />

	<div class="article-content">
		<a href="/hadoop"> Go to Home Back </a>
	</div>
</body>
</html>