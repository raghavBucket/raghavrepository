<!DOCTYPE html>
<html>
<head>
<title>Big Data POC</title>
<link rel="stylesheet" type="text/css" href="/hadoop/css/homestyle.css">
</head>
<body>
	<div class="image-wrap text-center">
		<div class="row w-100 no-gutters">
			<div class="col-12 align-self-center text-white p2">
				<div class="masthead-content">
					<div class="container">
						<h1 style="text-align: center; color: Red">Big Data & Apache
							Hadooop Practice</h1>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="article-content">
		<p style="text-align: left;">Big Data processing is a process of
			capturing and handling huge data from many sources using its tools ,
			frameworks and some techniques.</p>
	</div>
	<table>
		<tr>
			<td><div style="float: left;">
					<IMG SRC="image/mapreduce.jpg">
				</div></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>
				<div class="article-content">
					<strong><U>Use Cases List</U></strong>
					<ul>
						<li><a style="color: blue"
							href="wordcount/wordCountFileUpload.jsp">Word Count Analysis</a></li>
						<li><a style="color: blue"
							href="titanicship/titanicShipFileUpload.jsp">Titanic Ship
								Analysis</a></li>
						<li><a style="color: blue"
							href="employeesalary/employeeDataFileUpload.jsp">Employee
								Salary Analysis</a></li>
						<li><a style="color: blue"
							href="earthquake/earthQuakeFileUpload.jsp">Earth Quake
								Analysis</a></li>
						<li><a style="color: blue"
							href="travelpath/travelPathFileUpload.jsp">Travel Path
								Analysis</a></li>
						<li><a style="color: blue"
							href="trafficrules/trafficRulesFileUpload.jsp">Traffic Rules
								Analysis</a></li>
					</ul>
				</div>
			</td>
		</tr>
	</table>
</body>
</html>
