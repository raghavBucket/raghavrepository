package com.mapreduce.titanic.ship.analysis;

import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class TitanicShipAnalyser {

	public static class Map extends Mapper<LongWritable, Text, Text, IntWritable> {

		private Text gender = new Text();
		private IntWritable age = new IntWritable();

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String line = value.toString();
			String str[] = line.split(",");
			if (str.length > 6) {
				gender.set(str[4]);
				if ((str[1].equals("0"))) {
					if (str[5].matches("\\d+")) {
						int i = Integer.parseInt(str[5]);
						age.set(i);

					}
				}
			}
			context.write(gender, age);

		}

	}

	public static class Reduce extends Reducer<Text, IntWritable, Text, IntWritable> {

		public void reduce(Text key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			int l = 0;
			for (IntWritable val : values) {
				l += 1;
				sum += val.get();
			}
			sum = sum / l;
			context.write(key, new IntWritable(sum));
		}
	}

	public void compute(String sourcePath, String destinationPath) throws Exception {
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "wsord count");
		job.setJarByClass(TitanicShipAnalyser.class);
		job.setMapperClass(Map.class);
		job.setCombinerClass(Reduce.class);
		job.setReducerClass(Reduce.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(sourcePath));
		FileOutputFormat.setOutputPath(job, new Path(destinationPath));
		job.waitForCompletion(true);
	}

}
