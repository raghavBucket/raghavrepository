package com.mapreduce.travel.path.analysis;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class TravelPathAnalyser {

	public void compute(String sourcePath, String destinationPath, String searchString,String date) throws Exception {

		Configuration conf = new Configuration();
	
		conf.set("searchString",searchString);
		conf.set("date",date);
		Job job = Job.getInstance(conf, "Travel Path Analyser");

		job.setJarByClass(TravelPathAnalyser.class);
		job.setMapperClass(TravelPathMapper.class);

		job.setReducerClass(TravelPathReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		FileInputFormat.addInputPath(job, new Path(sourcePath));
		FileOutputFormat.setOutputPath(job, new Path(destinationPath));	        
		job.waitForCompletion(true);
	}
	
}