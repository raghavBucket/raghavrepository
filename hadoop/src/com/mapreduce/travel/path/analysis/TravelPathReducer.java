package com.mapreduce.travel.path.analysis;

import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;

public class TravelPathReducer extends Reducer<Text, Text, Text, Text> {

	@Override
	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		Configuration conf = context.getConfiguration();
		String searchString = conf.get("searchString");
		String date = conf.get("date");
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");

		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		HashMap<Date, String> valueMap = new HashMap<Date, String>();
		List<Date> list = new ArrayList<Date>();
		for (Text value : values) {
			String[] splited = value.toString().split("\\.");
			if (searchString.equals(key.toString())) {
				try {
					String splittedValue = splited[0].toString();
					Date dateValue = trim(sdf.parse(splittedValue));
					Date sarchDate = sdf1.parse(date);
					if (dateValue.equals(sarchDate)) {
						list.add(sdf.parse(splited[0]));
						valueMap.put(sdf.parse(splittedValue), splited[1]);
					}

				} catch (ParseException e1) {
					e1.printStackTrace();
				}
			}
		}

		Collections.sort(list);

		for (int i = 0; i < list.size(); ++i) {
			String path = valueMap.get(list.get(i));
			context.write(key, new Text( " | " +path +  " | " + list.get(i)));
		}
	}

	public static Date trim(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
}