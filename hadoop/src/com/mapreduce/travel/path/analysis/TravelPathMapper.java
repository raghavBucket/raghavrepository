package com.mapreduce.travel.path.analysis;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class TravelPathMapper extends Mapper<LongWritable, Text, Text, Text> {
	@Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String[] line = value.toString().split(",", 12);
		if (line.length == 6) {
			return;
		}
		String outputKey = line[0];
		String timestamp = line[2];
		String outputValue = line[3];
		context.write(new Text(outputKey), new Text(timestamp +"." + outputValue));
	}
}
