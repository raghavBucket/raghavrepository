package com.mapreduce.travel.traffic.rules.breaking.analysis;

import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;

public class TrafficRulesReducer extends Reducer<Text, Text, Text, Text> {

	@Override
	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		Configuration conf = context.getConfiguration();
		String searchString = conf.get("searchString");
		int warnedCount = 0;
		int caseFiledcount = 0;
		String content = "";
		if (searchString.equals(key.toString())) {
			for (Text value : values) {

				String[] splited = value.toString().split(" ");
				String isWarned = splited[0].toString();
				String isCaseFiled = splited[1].toString();

				if ("yes".equalsIgnoreCase(isWarned)) {
					warnedCount++;
				}
				if ("yes".equalsIgnoreCase(isCaseFiled)) {
					caseFiledcount++;
				}

			}
			content = "For this Licence No, " + searchString + " there are " + warnedCount + " times warnings given and "
					+ caseFiledcount + " times cases filed";
			context.write(key, new Text(content));
		} 
	}
}