package com.mapreduce.travel.traffic.rules.breaking.analysis;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class TrafficRulesMapper extends Mapper<LongWritable, Text, Text, Text> 
{
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] line = value.toString().split(",", 12);
        if (line.length == 8) {
            return;
        }
        String outputKey = line[0];
        String outputValue =line[2]+" "+line[3];
        context.write(new Text(outputKey), new Text(outputValue));
    }
}


