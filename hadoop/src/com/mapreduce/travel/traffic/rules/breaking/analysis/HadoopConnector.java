package com.mapreduce.travel.traffic.rules.breaking.analysis;


import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Properties;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

import com.mapreduce.travel.traffic.rules.breaking.analysis.TrafficRulesAnalyser;

public class HadoopConnector {

	public String savetoHdfs(String filepath, String fileName,String searchString) throws Exception {
		Properties prop = new Properties();
		InputStream inStream = this.getClass().getResourceAsStream("/application.properties");
		prop.load(inStream);
		

		String inputDirectory = prop.getProperty("hadoop.input.url");
		String outputDirectory = prop.getProperty("hadoop.output.url");
		String hdfsInstancePort = prop.getProperty("hdfs.instance.port");
		String hdfsFileDownloadUrl1 = prop.getProperty("hdfs.file.download.url1");
		String hdfsFileDownloadUrl2 = prop.getProperty("hdfs.file.download.url2");
		
		String sourcePath = inputDirectory + fileName;
		String desitinationPath = outputDirectory+fileName.replace(".txt", "");
		
		Path inputFilename = new Path(new URI(sourcePath));

		Configuration conf = new Configuration();
		conf.set("fs.defaultFS", inputDirectory);
		FileSystem fs = FileSystem.get(conf);
		OutputStream os = fs.create(inputFilename);
		
		InputStream is = new BufferedInputStream(new FileInputStream(filepath));
		IOUtils.copyBytes(is, os, conf);

		if (fs.exists(new Path(new URI(desitinationPath)))) {
			fs.delete(new Path(new URI(desitinationPath)), true);
		}
		
		TrafficRulesAnalyser trafficRulesAnalyser = new TrafficRulesAnalyser();
		trafficRulesAnalyser.compute(sourcePath, desitinationPath, searchString);
		
		return desitinationPath.replace(hdfsInstancePort, hdfsFileDownloadUrl1) + hdfsFileDownloadUrl2;
	}

}