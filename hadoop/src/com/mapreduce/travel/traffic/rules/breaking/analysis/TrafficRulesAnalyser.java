package com.mapreduce.travel.traffic.rules.breaking.analysis;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class TrafficRulesAnalyser {

	public void compute(String sourcePath, String destinationPath, String searchString) throws Exception {

		Configuration conf = new Configuration();

		conf.set("searchString", searchString);
		Job job = Job.getInstance(conf, "Traffic Rules Analyser");

		job.setJarByClass(TrafficRulesAnalyser.class);
		job.setMapperClass(TrafficRulesMapper.class);

		job.setReducerClass(TrafficRulesReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		FileInputFormat.addInputPath(job, new Path(sourcePath));
		FileOutputFormat.setOutputPath(job, new Path(destinationPath));
		job.waitForCompletion(true);
	}

}