package com.mapreduce.earthquake.analysis;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class EarthquakeAnalyser {

	public void compute(String sourcePath, String destinationPath) throws Exception {
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "Earth Quake Analyser");
		job.setJarByClass(EarthquakeAnalyser.class);
		job.setMapperClass(EarthquakeMapper.class);

		job.setReducerClass(EarthquakeReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(DoubleWritable.class);
		FileInputFormat.addInputPath(job, new Path(sourcePath));
		FileOutputFormat.setOutputPath(job, new Path(destinationPath));
		job.waitForCompletion(true);
	}
}