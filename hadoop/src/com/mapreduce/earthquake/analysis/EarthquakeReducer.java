package com.mapreduce.earthquake.analysis;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;
import org.apache.hadoop.io.Text;


public class EarthquakeReducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {
	String oldkey=null;
	@Override
	public void reduce(Text key, Iterable<DoubleWritable> values, Context context)
			throws IOException, InterruptedException {
		double maxMagnitude = 0.0;
		for (DoubleWritable value : values) {
			maxMagnitude = Math.max(maxMagnitude, value.get());
		}
		context.write(key, new DoubleWritable(maxMagnitude));
	}
}